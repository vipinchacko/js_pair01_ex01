import Chai from 'chai';
import Rectangle from '../src/rectangle';

let expect = Chai.expect;

describe("Rectangle", () => {
    
    //test cases for method returning area of rectangle
    describe("Area", () => {
        it('should return area 12 when length is 3 and breadth is 4', () => {
            expect(12).to.eq(12);
        })
        it('should return area 20 when length is 4 and breadth is 5', () => {
            expect(new Rectangle(4, 5).area()).to.eq(20);
        })
    })

    //test cases for method returning perimeter of rectangle
    describe("Perimeter", () => {
        it('should return perimeter 14 when length is 3 and breadth is 4', () => {
            expect(new Rectangle(3, 4).perimeter()).to.eq(14);
        })
        it('should return perimeter 18 when length is 4 and breadth is 5', () => {
            expect(new Rectangle(4, 5).perimeter()).to.eq(18);
        })
    })
})