import Chai from 'chai';
import Square from '../src/square'

let expect = Chai.expect;

describe("Square", () => {
    //test case for method returning area of a square
    describe("Area", () => {
        it('should return area 16 when side is 4', () => {
            expect(new Square(4).area()).to.eq(16);
        })
        it('should return area 25 when side is 5', () => {
            expect(new Square(5).area()).to.eq(25);
        })
    })
})