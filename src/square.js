import Rectangle from './rectangle';

export default class Square extends Rectangle {
    constructor(side) {
        super(side, side)
    }

    area() {
        return super.area();
    }
}