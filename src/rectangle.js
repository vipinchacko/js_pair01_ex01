export default class Rectangle {
    constructor(length,breadth){
        this.length = length;
        this.breadth = breadth;        
    }
    //method to return area of rectangle
    area() {
        return this.length * this.breadth;
    }

    //method to return perimeter of rectangle
    perimeter() {
        return 2 * this.length + 2 * this.breadth;
    }
}
